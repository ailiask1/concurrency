package multithreading;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadAction {

    private static final ReentrantLock lock = new ReentrantLock(true);
    final static Logger logger = LogManager.getLogger();
    private int[][] matrix;

    public ThreadAction(int[][] matrix) {
        this.matrix = matrix;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public void startThread() {
        Random random = new Random();
        int randomNumber = random.nextInt(100) + 1;
        String threadName = Thread.currentThread().getName();
        logger.info("Thread '" + threadName + "' was created. It has random number " + randomNumber);
        try {
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException e) {
            logger.log(Level.ERROR, "Interrupted", e);
        }
        replaceMatrixElements(threadName, randomNumber);
    }

    public void replaceMatrixElements(String threadName, int randomNumber) {
        for (int i = 0; i < matrix.length; i++) {
            lock.lock();
            if (matrix[i][i] == 0) {
                matrix[i][i] = randomNumber;
                logger.debug("Diagonal element " + (i + 1) + " was changed by " + threadName + " with number " + randomNumber);
                lock.unlock();
                try {
                    TimeUnit.MILLISECONDS.sleep(10);
                } catch (InterruptedException e) {
                    logger.log(Level.ERROR, "Interrupted", e);
                }
            } else {
                lock.unlock();
            }
        }
    }
}

