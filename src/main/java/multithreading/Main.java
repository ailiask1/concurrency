package multithreading;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    final static Logger logger = LogManager.getLogger();
    private static String INPUTFILE = "data\\input.txt";
    private static String OUTPUTFILE = "data\\output.txt";

    public static void main(String[] args) {

        try {
            logger.info("Start of reading arguments from the file.");
            int[] numbers = readFile(INPUTFILE);
            if (isValidNumbers(numbers)) {
                logger.info("Arguments were successfully taken from the file.");
                int sizeOfMatrix = numbers[0];
                int numberOfThreads = numbers[1];
                int[][] matrix = new int[sizeOfMatrix][sizeOfMatrix];
                ThreadAction threadAction = new ThreadAction(matrix);
                logger.info("Start of the multithreading.");
                List<Thread> threads = new ArrayList<>();
                for (int i = 0; i < numberOfThreads; i++) {
                    Thread thread = new Thread(threadAction::startThread);
                    thread.start();
                    threads.add(thread);
                }
                for (Thread thread : threads) {
                    thread.join();
                }
                logger.info("Start of writing matrix into the file.");
                writeMatrix(OUTPUTFILE, threadAction.getMatrix());
            }
        } catch (FileNotFoundException e) {
            logger.log(Level.ERROR, "File was not founded", e);
        } catch (IOException e) {
            logger.log(Level.ERROR, "Error with writing matrix into file.", e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static int[] readFile(String directory) throws FileNotFoundException {
        File file = new File(directory);
        Scanner scanner = new Scanner(file);
        int count = 0;
        while (scanner.hasNextInt()) {
            count++;
            scanner.nextInt();
        }
        int[] numbers = new int[count];
        Scanner scanner1 = new Scanner(file);
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = scanner1.nextInt();
        }
        return numbers;
    }

    private static boolean isValidNumbers(int[] numbers) {
        boolean validNumber = false;
        if (numbers.length < 2) {
            logger.error("Not enough arguments. Check the file \"input.txt\".");
        } else if (numbers.length > 2) {
            logger.error("There should be only 2 arguments. Check the file \"input.txt\".");
        } else if (numbers[0] < 8 || numbers[0] > 12) {
            logger.error("The size of the matrix should be in the range of 8-12. Check the file \"input.txt\".");
        } else if (numbers[1] < 4 || numbers[1] > 6) {
            logger.error("The amount of the threads should be in the range of 4-6. Check the file \"input.txt\".");
        } else {
            validNumber = true;
        }
        return validNumber;
    }

    private static void clearFile(String directory) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(directory);
        writer.print("");
        writer.close();
    }

    private static void writeMatrix(String directory, int[][] matrix) throws IOException {
        clearFile(directory);
        BufferedWriter writer = new BufferedWriter(new FileWriter(directory));
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                writer.write(matrix[i][j] + " ");
            }
            writer.newLine();
        }
        writer.flush();
        writer.close();
        logger.info("Matrix was successfully written into the file.");
    }
}
